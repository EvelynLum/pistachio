using System;

namespace Pistachio.BLLs
{
    public interface IPayoutCalculator
    {
        decimal CalculateWinAmount(decimal betAmount, decimal rate);
    }

    public class PayoutCalculator : IPayoutCalculator
    {
        public PayoutCalculator()
        {
        }

        public decimal CalculateWinAmount(decimal betAmount, decimal rate)
        {
            if (betAmount < 0 || rate < 0)
            {
                throw new Exception("Bet amount should be greater than zero.");
            }

            return (betAmount * rate);
        }
    }
}