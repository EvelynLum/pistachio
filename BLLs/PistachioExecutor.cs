using Pistachio.Models.Requests;
using Pistachio.Models.BLLModels;
using System.Collections.Generic;
using System.Linq;
using System;
using Poker.Core.Models;
using Pistchio.BLLs;
using PokerHands.Enums;
using pistachio.Models.BLLModels;

namespace Pistachio.BLLs
{
    public interface IPistachioExecutor
    {
        PistachioBLLModel Execute(PistachioRequest req);
    }

    public class PistachioExecutor : IPistachioExecutor
    {
        const short NUMBER_OF_CARD_DECK = 1;
        const short NUMBER_OF_JOKER = 1;

        private IShuffleMachine _shuffleMachine;
        private IHandsMatcher _handMatcher;
        private IPayoutCalculator _payoutCalculator;

        public PistachioExecutor(IShuffleMachine shuffleMachine,
                                 IHandsMatcher handsMatcher,
                                 IPayoutCalculator payoutCalculator)
        {
            _shuffleMachine = shuffleMachine;
            _handMatcher = handsMatcher;
            _payoutCalculator = payoutCalculator;
        }

        public PistachioBLLModel Execute(PistachioRequest req)
        {
            Validate(req);
            List<PokerCard> shuffledDeck = _shuffleMachine.ShuffleCardDeck(NUMBER_OF_CARD_DECK, NUMBER_OF_JOKER);
            List<PokerCard> first5Cards = shuffledDeck.Take(5).ToList();
            HandResult result = _handMatcher.GetMatchedResult(first5Cards);
            decimal winAmount = _payoutCalculator.CalculateWinAmount(req.BetAmount, result.Rate);

            return new PistachioBLLModel()
            {
                PokerCards = first5Cards,
                Pattern = result.Pattern,
                WinAmount = winAmount
            };
        }

        private void Validate(PistachioRequest req)
        {
            if (req.BetAmount < 0)
            {
                throw new Exception("Bet amount shuld not lesser than zero.");
            }
        }
    }
}