using Poker.Core.Enums;
using Poker.Core.Models;
using System;
using System.Collections.Generic;

namespace Pistchio.BLLs
{
    public interface IShuffleMachine
    {
        List<PokerCard> ShuffleCardDeck(short numberOfDeck, short numberOfJoker);
    }

    public class ShuffleMachine : IShuffleMachine
    {
        private static Random _random = new Random();

        private readonly List<PokerCard> _card_deck_without_joker = new List<PokerCard>()
        {
            new PokerCard(){ Suit = Suit.Spade, FaceValue = FaceValue.Ace },
            new PokerCard(){ Suit = Suit.Spade, FaceValue = FaceValue.Two },
            new PokerCard(){ Suit = Suit.Spade, FaceValue = FaceValue.Three },
            new PokerCard(){ Suit = Suit.Spade, FaceValue = FaceValue.Four },
            new PokerCard(){ Suit = Suit.Spade, FaceValue = FaceValue.Five },
            new PokerCard(){ Suit = Suit.Spade, FaceValue = FaceValue.Six },
            new PokerCard(){ Suit = Suit.Spade, FaceValue = FaceValue.Seven },
            new PokerCard(){ Suit = Suit.Spade, FaceValue = FaceValue.Eight },
            new PokerCard(){ Suit = Suit.Spade, FaceValue = FaceValue.Nine },
            new PokerCard(){ Suit = Suit.Spade, FaceValue = FaceValue.Ten },
            new PokerCard(){ Suit = Suit.Spade, FaceValue = FaceValue.Jack },
            new PokerCard(){ Suit = Suit.Spade, FaceValue = FaceValue.Queen },
            new PokerCard(){ Suit = Suit.Spade, FaceValue = FaceValue.King },
            new PokerCard(){ Suit = Suit.Heart, FaceValue = FaceValue.Ace },
            new PokerCard(){ Suit = Suit.Heart, FaceValue = FaceValue.Two },
            new PokerCard(){ Suit = Suit.Heart, FaceValue = FaceValue.Three },
            new PokerCard(){ Suit = Suit.Heart, FaceValue = FaceValue.Four },
            new PokerCard(){ Suit = Suit.Heart, FaceValue = FaceValue.Five },
            new PokerCard(){ Suit = Suit.Heart, FaceValue = FaceValue.Six },
            new PokerCard(){ Suit = Suit.Heart, FaceValue = FaceValue.Seven },
            new PokerCard(){ Suit = Suit.Heart, FaceValue = FaceValue.Eight },
            new PokerCard(){ Suit = Suit.Heart, FaceValue = FaceValue.Nine },
            new PokerCard(){ Suit = Suit.Heart, FaceValue = FaceValue.Ten },
            new PokerCard(){ Suit = Suit.Heart, FaceValue = FaceValue.Jack },
            new PokerCard(){ Suit = Suit.Heart, FaceValue = FaceValue.Queen },
            new PokerCard(){ Suit = Suit.Heart, FaceValue = FaceValue.King },
            new PokerCard(){ Suit = Suit.Club, FaceValue = FaceValue.Ace },
            new PokerCard(){ Suit = Suit.Club, FaceValue = FaceValue.Two },
            new PokerCard(){ Suit = Suit.Club, FaceValue = FaceValue.Three },
            new PokerCard(){ Suit = Suit.Club, FaceValue = FaceValue.Four },
            new PokerCard(){ Suit = Suit.Club, FaceValue = FaceValue.Five },
            new PokerCard(){ Suit = Suit.Club, FaceValue = FaceValue.Six },
            new PokerCard(){ Suit = Suit.Club, FaceValue = FaceValue.Seven },
            new PokerCard(){ Suit = Suit.Club, FaceValue = FaceValue.Eight },
            new PokerCard(){ Suit = Suit.Club, FaceValue = FaceValue.Nine },
            new PokerCard(){ Suit = Suit.Club, FaceValue = FaceValue.Ten },
            new PokerCard(){ Suit = Suit.Club, FaceValue = FaceValue.Jack },
            new PokerCard(){ Suit = Suit.Club, FaceValue = FaceValue.Queen },
            new PokerCard(){ Suit = Suit.Club, FaceValue = FaceValue.King },
            new PokerCard(){ Suit = Suit.Diamond, FaceValue = FaceValue.Ace },
            new PokerCard(){ Suit = Suit.Diamond, FaceValue = FaceValue.Two },
            new PokerCard(){ Suit = Suit.Diamond, FaceValue = FaceValue.Three },
            new PokerCard(){ Suit = Suit.Diamond, FaceValue = FaceValue.Four },
            new PokerCard(){ Suit = Suit.Diamond, FaceValue = FaceValue.Five },
            new PokerCard(){ Suit = Suit.Diamond, FaceValue = FaceValue.Six },
            new PokerCard(){ Suit = Suit.Diamond, FaceValue = FaceValue.Seven },
            new PokerCard(){ Suit = Suit.Diamond, FaceValue = FaceValue.Eight },
            new PokerCard(){ Suit = Suit.Diamond, FaceValue = FaceValue.Nine },
            new PokerCard(){ Suit = Suit.Diamond, FaceValue = FaceValue.Ten },
            new PokerCard(){ Suit = Suit.Diamond, FaceValue = FaceValue.Jack },
            new PokerCard(){ Suit = Suit.Diamond, FaceValue = FaceValue.Queen },
            new PokerCard(){ Suit = Suit.Diamond, FaceValue = FaceValue.King },
        };


        public List<PokerCard> ShuffleCardDeck(short numberOfDeck, short numberOfJoker)
        {
            if (numberOfDeck < 0)
            {
                throw new Exception("Number of Deck should be greater or equal to zero.");
            }
            if (numberOfJoker < 0)
            {
                throw new Exception("Number of Joker should be greater or equal to zero.");
            }

            List<PokerCard> cardDeck = PrepareCardDeck(numberOfDeck, numberOfJoker);
            Shuffle(cardDeck);

            return cardDeck;
        }

        private List<PokerCard> PrepareCardDeck(short numberOfDeck, short numberOfJoker)
        {
            List<PokerCard> cardDeck = new List<PokerCard>();
            for (int i = 0; i < numberOfDeck; i++)
            {
                cardDeck.AddRange(_card_deck_without_joker);
            }

            for (int i = 0; i < numberOfJoker; i++)
            {
                cardDeck.Add(new PokerCard() { Suit = Suit.Joker, FaceValue = FaceValue.Joker });
            }

            return cardDeck;
        }

        private void Shuffle(List<PokerCard> cardDeck)
        {
            int totalCards = cardDeck.Count;
            int lastPosition = cardDeck.Count - 1;
            PokerCard swapCardHolder = new PokerCard();

            for (int i = 0; i < lastPosition; i++)
            {
                int swapPosition = i + _random.Next(totalCards - i);

                swapCardHolder = cardDeck[swapPosition];
                cardDeck[swapPosition] = cardDeck[i];
                cardDeck[i] = swapCardHolder;
            }
        }
    }
}