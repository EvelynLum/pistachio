﻿using pistachio.Models.BLLModels;
using Poker.Core.Models;
using PokerHands.Core;
using PokerHands.Enums;
using PokerHands.Hands;
using PokerHands.Interfaces;
using System.Collections.Generic;

namespace Pistachio.BLLs
{
    public interface IHandsMatcher
    {
        Pattern FindPattern(List<PokerCard> pokerCards);
        HandResult GetMatchedResult(List<PokerCard> pokerCards);
        HandResult GetResult(Pattern pattern);
    }

    public class HandsMatcher : IHandsMatcher
    {
        CardProcessor _processor = new CardProcessor();

        private static readonly Dictionary<Pattern, HandResult> _patternAndHandResultPairs = new Dictionary<Pattern, HandResult>()
        {
            { Pattern.None, new HandResult(){ Pattern = Pattern.None, Rate = 0 } },
            { Pattern.OnePair, new HandResult(){ Pattern = Pattern.OnePair, Rate = 1 } },
            { Pattern.TwoPair, new HandResult(){ Pattern = Pattern.TwoPair, Rate = 2 } },
            { Pattern.ThreeOfAKind, new HandResult(){ Pattern = Pattern.ThreeOfAKind, Rate = 3 } },
            { Pattern.Straight, new HandResult(){ Pattern = Pattern.Straight, Rate = 4 } },
            { Pattern.Flush, new HandResult(){ Pattern = Pattern.Flush, Rate = 5 } },
            { Pattern.FullHouse, new HandResult(){ Pattern = Pattern.FullHouse, Rate = 6 } },
            { Pattern.FourOfAKind, new HandResult(){ Pattern = Pattern.FourOfAKind, Rate = 7 } },
            { Pattern.StraightFlush, new HandResult(){ Pattern = Pattern.StraightFlush, Rate = 8 } },
            { Pattern.RoyalFlush, new HandResult(){ Pattern = Pattern.RoyalFlush, Rate = 9 } },
            { Pattern.FiveOfAKind, new HandResult(){ Pattern = Pattern.FiveOfAKind, Rate = 10 } },
        };

        public HandsMatcher()
        {
        }

        public HandResult GetMatchedResult(List<PokerCard> pokerCards)
        {
            return GetResult(FindPattern(pokerCards));
        }

        public Pattern FindPattern(List<PokerCard> pokerCards)
        {
            IHand fiveOfAKind = new FiveOfAKind();
            IHand straightFlush = new StraightFlush();
            IHand fourOfAKind = new FourOfAKind();
            IHand flush = new Flush();
            IHand straight = new Straight();
            IHand threeOfAKind = new ThreeOfAKind();
            IHand onePair = new OnePair();
            IHand none = new None();

            fiveOfAKind.SetNext(straightFlush);
            straightFlush.SetNext(fourOfAKind);
            fourOfAKind.SetNext(flush);
            flush.SetNext(straight);
            straight.SetNext(threeOfAKind);
            threeOfAKind.SetNext(onePair);
            onePair.SetNext(none);

            var groupBySuit = _processor.GroupBySuit(pokerCards);
            var groupByFaceValue = _processor.GroupByFaceValue(pokerCards);
            return fiveOfAKind.MatchInRank(groupBySuit, groupByFaceValue);
        }

        public HandResult GetResult(Pattern pattern)
        {
            switch (pattern)
            {
                case Pattern.OnePair:
                case Pattern.TwoPair:
                case Pattern.ThreeOfAKind:
                case Pattern.Straight:
                case Pattern.Flush:
                case Pattern.FullHouse:
                case Pattern.FourOfAKind:
                case Pattern.StraightFlush:
                case Pattern.RoyalFlush:
                case Pattern.FiveOfAKind:
                    return _patternAndHandResultPairs[pattern];
                default:
                    return _patternAndHandResultPairs[Pattern.None];
            }
        }
    }
}
