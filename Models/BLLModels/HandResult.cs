﻿using PokerHands.Enums;

namespace pistachio.Models.BLLModels
{
    public class HandResult
    {
        public Pattern Pattern { get; set; }
        public decimal Rate { get; set; }
    }
}
