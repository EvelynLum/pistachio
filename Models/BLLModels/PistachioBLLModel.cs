using Poker.Core.Models;
using PokerHands.Enums;
using System.Collections.Generic;

namespace Pistachio.Models.BLLModels
{
    public class PistachioBLLModel
    {
        public List<PokerCard> PokerCards { get; set; }
        public Pattern Pattern { get; set; }
        public decimal WinAmount { get; set; }
    }
}